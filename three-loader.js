window.addEventListener('load', function () {
    const containers = document.querySelectorAll(".main-direction__3d");

    if (containers.length > 0) {
        const doc_width = document.body.clientWidth;

        setTimeout(() => {

            const l = new Loader();
            l.require([
                    site_url + "wp-content/themes/true/assets/js/three.min.js",
                    site_url + "wp-content/themes/true/assets/js/OrbitControls.js",
                    site_url + "wp-content/themes/true/assets/js/fflate.min.js",
                    site_url + "wp-content/themes/true/assets/js/FBXLoader.js"
                ],

                function () {

                    setTimeout(() => {

                        const containers = document.querySelectorAll(".main-direction__3d");

                        for (let i = 0; i < containers.length; i++) {

                            const canvas = containers[i].getElementsByTagName("canvas")[0];
                            const model = containers[i].dataset.three;

                            if (model.includes('http')) {
                                function init() {
                                    const {
                                        width,
                                        height
                                    } = canvas.getBoundingClientRect();
                                    // 1. Setup scene
                                    const scene = new THREE.Scene();
                                    // 2. Setup camera
                                    const camera = new THREE.PerspectiveCamera(75, width / height);
                                    // 3. Setup renderer
                                    const renderer = new THREE.WebGLRenderer({
                                        canvas,
                                        antialias: true,
                                    });
                                    renderer.setSize(width, height);

                                    // light
                                    const color = 0xFFFFFF;
                                    // const intensity = .6;
                                    const light = new THREE.DirectionalLight(color, .4);
                                    light.position.set(0, 5, 0);
                                    light.target.position.set(-5, 2, -12);
                                    scene.add(light);
                                    scene.add(light.target);
                                    const light_one = new THREE.DirectionalLight(color, .6);
                                    light_one.position.set(0, 10, 0);
                                    light_one.target.position.set(-5, 2, 15);
                                    scene.add(light_one);
                                    scene.add(light_one.target);
                                    const light3 = new THREE.AmbientLight(0x404040, 2); // soft white light
                                    scene.add(light3);

                                    // camera
                                    // 4. Use requestAnimationFrame to recursively draw the scene in the DOM.
                                    camera.orbitControls = new THREE.OrbitControls(camera, canvas);
                                    camera.orbitControls.enableKeys = false;
                                    camera.orbitControls.enablePan = false;
                                    camera.orbitControls.enableZoom = false;
                                    camera.orbitControls.enableDamping = false;
                                    camera.orbitControls.enableRotateX = true;
                                    camera.orbitControls.autoRotate = true;
                                    // camera.position.set(0.8, 1.4, 1.0);
                                    camera.position.z = -220;
                                    camera.position.y = 0;
                                    camera.position.x = 0;

                                    // get models
                                    const fbxLoader = new THREE.FBXLoader();
                                    fbxLoader.load(
                                        model,
                                        (object) => {

                                            // center
                                            let box = new THREE.Box3().setFromObject(object);
                                            box.getCenter(object.position); // this re-sets the mesh position
                                            object.position.multiplyScalar(-1);
                                            let pivot = new THREE.Group();
                                            scene.add(pivot);
                                            pivot.add(object);
                                            // center end
                                            containers[i].removeAttribute('style');
                                        },
                                        (xhr) => {
                                            console.log((xhr.loaded / xhr.total) * 100 + '% loaded');
                                        },
                                        (error) => {
                                            console.log(error)
                                        }
                                    )

                                    // движение
                                    function animate() {
                                        // orbitControls.autoRotate is enabled so orbitControls.update
                                        // must be called inside animation loop.
                                        camera.orbitControls.update();
                                        requestAnimationFrame(animate);
                                        renderer.render(scene, camera);

                                            window.onresize = function(event) {

                                                if(doc_width != document.body.clientWidth ) {
                                                    setTimeout(() => {

                                                        const resizecanvas = containers[i].getElementsByTagName("canvas")[0];
                                                        const {
                                                            width,
                                                            height
                                                        } = resizecanvas.getBoundingClientRect();
        
                                                        camera.aspect = width / height;
                                                        camera.updateProjectionMatrix();
        
                                                        renderer.setSize(width, height);
                                                    }, 100);
        
                                                }


                                            };
                                          
                                    }

                                    animate();
                                }

                                function hasWebGL() {
                                    const gl =
                                        canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
                                    if (gl && gl instanceof WebGLRenderingContext) {
                                        return true;
                                    } else {
                                        return false;
                                    }
                                }

                                if (hasWebGL()) {
                                    init();
                                }
                            }

                        }

                    }, 100);

                });

        }, 100);

    }
});

window.addEventListener('scroll', function() {
    const containers = document.querySelectorAll(".main-direction__3d");

    for (let i = 0; i < containers.length; i++) {
        const canvas = containers[i].getElementsByTagName("canvas")[0];
    }
});

// Sequential loading of scripts
class Loader {
    constructor() {}
    require(scripts, callback) {
        this.loadCount = 0;
        this.totalRequired = scripts.length;
        this.callback = callback;

        for (let i = 0; i < scripts.length; i++) {
            this.writeScript(scripts[i]);
        }
    }
    loaded(evt) {
        this.loadCount++;

        if (this.loadCount == this.totalRequired && typeof this.callback == 'function')
            this.callback.call();
    }
    writeScript(src) {
        let self = this;
        let s = document.createElement('script');
        s.type = "text/javascript";
        s.async = false;
        s.src = src;
        s.addEventListener('load', function (e) {
            self.loaded(e);
        }, false);
        let head = document.getElementsByTagName('head')[0];
        head.appendChild(s);
    }
}