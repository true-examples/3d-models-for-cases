<?php
if( have_rows('cases') ): ?>
  <?php $i = 0; ?>
  <?php while( have_rows('cases') ) : the_row(); ?>

    <?php
      $title = get_sub_field('title');
      $direction = get_sub_field('direction');
      $link = get_sub_field('filter_link');
      $case = get_permalink(get_sub_field('case')->ID);
      $description = get_sub_field('description');
      $model = get_sub_field('3d_model')['url'];
      $model_screen = get_sub_field('3d_model_screen')['url'];
    ?>

    <div class="page page--top page--direction" data-forward="rotate" data-back="rotate" data-speed="600">

      <div class="container container--h100">
        <div class="page__wrapper">
          <div class="page__container page__container--center  page__container--main ">
            <div class="main-direction" >
              <div class="main-direction__left">
                <div class="main-direction__direction">
                  <h6 class="h6"> 
                    <?php if(get_sub_field('filter_link')):?>
                      <a href="<?php the_sub_field('filter_link');?>" class="main-direction__filter"><?= $direction ?></a>
                    <?php else:?>
                      <span class="main-direction__filter"><?= $direction ?></span>
                    <?php endif;?>
                  </h6>
                  <div class="main-direction__number">
                    0<?= ++$i ?>
                  </div>
                </div>
                <div class="main-direction__title">
                  <h2 class="h1"><?= $title ?></h2>
                </div>
                <div class="main-direction__desc editor">
                  <?= $description ?>
                </div>
                <div class="main-direction__button">
                  <a href="<?= $case ?>" class="button">
                    <span><?php echo ($languageDetection) ? 'More' : 'Подробнее' ?></span>
                    <span class="arrow">
                      <img src="<?= get_template_directory_uri()  ?>/assets/images/static/next.svg" alt="">
                    </span>
                  </a>
                </div>
              </div>
              <div class="main-direction__right">           

                <?php 
                  $model = get_sub_field('3d_model')['url'];
                  $icon = get_sub_field('3d_model_screen')['sizes']['my-mini'];
                ?>
                <div class="main-direction__3d main-direction__3d--new" data-three="<?=$model?>" style="background-image: url(<?= $icon?>);">
                  <canvas class="main-direction__canvas"></canvas>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  <?php endwhile; ?>
<?php endif; ?>